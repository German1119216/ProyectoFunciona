//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lisandro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class fact_ven
    {
        public fact_ven()
        {
            this.detalle_ven = new HashSet<detalle_ven>();
        }
    
        public int Ref_Fact_Ven { get; set; }
        public System.DateTime Fecha { get; set; }
        public byte[] Pago { get; set; }
        public int FK_Id_Emp { get; set; }
        public int Fk_Presentacion_Producto { get; set; }
        public int Fk_Id_Usu { get; set; }
    
        public virtual ICollection<detalle_ven> detalle_ven { get; set; }
        public virtual empleado empleado { get; set; }
        public virtual presentacion_producto presentacion_producto { get; set; }
        public virtual usuario usuario { get; set; }
    }
}
