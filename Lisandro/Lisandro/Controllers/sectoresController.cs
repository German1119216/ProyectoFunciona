﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lisandro.Models;

namespace Lisandro.Controllers
{
    public class sectoresController : Controller
    {
        private swEntities db = new swEntities();

        //
        // GET: /sectores/

        public ActionResult Index()
        {
            return View(db.sectors.ToList());
        }

        //
        // GET: /sectores/Details/5

        public ActionResult Details(int Ref_Sector = 0)
        {
            sector sector = db.sectors.Find(Ref_Sector);
            if (sector == null)
            {
                return HttpNotFound();
            }
            return View(sector);
        }

        //
        // GET: /sectores/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /sectores/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(sector sector)
        {
            if (ModelState.IsValid)
            {
                db.sectors.Add(sector);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sector);
        }

        //
        // GET: /sectores/Edit/5

        public ActionResult Edit(int Ref_Sector = 0)
        {
            sector sector = db.sectors.Find(Ref_Sector);
            if (sector == null)
            {
                return HttpNotFound();
            }
            return View(sector);
        }

        //
        // POST: /sectores/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(sector sector)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sector).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sector);
        }

        //
        // GET: /sectores/Delete/5

        public ActionResult Delete(int Ref_Sector = 0)
        {
            sector sector = db.sectors.Find(Ref_Sector);
            if (sector == null)
            {
                return HttpNotFound();
            }
            return View(sector);
        }

        //
        // POST: /sectores/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int Ref_Sector)
        {
            sector sector = db.sectors.Find(Ref_Sector);
            db.sectors.Remove(sector);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}